var divs = document.getElementById('elementos-cat');

function autorun() {
    fetch('/src/js/categories.json')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var vacio = '';
            myJson.categories.forEach(element => {
                divs.innerHTML += `<div class="${element.name}"  id="${element.name}">
                <div class="text">
                <img src="./images/categories/${element.image}" class='nombre'>
                    <h2>${element.name}</h2>
                </div></div>`;
            });
        });
}

autorun();

var svid= document.getElementById('elementos-pro');
function autorunDos() {
    fetch('/src/js/products.json')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var vacio = '';
            myJson.products.forEach(element => {
                svid.innerHTML += `<div class="${element.name}"  id="${element.name}">
                <div class="text">
                <img src="./images/products/${element.image}" class='nombre'>
                    <h2>${element.name}</h2>
                </div></div>`;
            });
        });
}
autorunDos();